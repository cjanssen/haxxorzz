Police = {}

function Police.init()
	Police.timer = 0
	Police.safeTime = 3
	Police.alertTime = 1
	Police.abortTime = 0
	Police.traceTime = 1
	Police.detectTime = 3.5
	Police.goneTime = 1
	Police.foundTime = 4
	Police.timer = 0
	Police.state = "safe"
	Police.mode = "scan"
	Police.x = 500
	Police.y = 15
	Police.keydown = false
	Police.active = false
	Police.lastcall = false
	Police.blink = true
	Police.blinkPeriod = 1
	Police.acceptsKeys = true
	Police.expectsKey = false
end

function Police.keyPressed()
	if Police.acceptsKeys and Police.expectsKey then
		Police.keydown = true
		return true
	end
end

function Police.update( dt )
	if not Police.active then
		return
	end

	Police.timer = Police.timer + dt
	if Police.timer % Police.blinkPeriod < Police.blinkPeriod * 0.5 then
		Police.blink = true
	else
		Police.blink = false
	end
	
	Police.expectsKey = false

	if Police.state == "safe" then
		if Police.timer >= Police.safeTime then
			if Police.lastcall then
				Police.active = false
			else
				Police.setState("alert")
			end
		end
	elseif Police.state == "alert" then
		Police.expectsKey = true
		if Police.timer >= Police.alertTime then
			Police.setState("aborting")
		end
		if Police.keydown then
			Police.setState("tracing")
		end
	elseif Police.state == "aborting" then
		if Police.timer >= Police.abortTime then
			Police.setState("safe")
		end
	elseif Police.state == "tracing" then
		if Police.timer >= Police.traceTime then
			Police.setState("detect")
		end
	elseif Police.state == "detect" then
		Police.expectsKey = true
		if Police.timer >= Police.detectTime then
			Police.setState("gone")
		end
		if Police.keydown or Police.mode == "request" then
			Police.setState("found")
		end
	elseif Police.state == "found" then
		if Police.timer >= Police.foundTime then
			Police.setState("reboot")
		end
	elseif Police.state == "gone" then
		if Police.timer >= Police.goneTime then
			Police.setState("safe")
		end
	end
	Police.keydown = false
end

function Police.setState(newState)
	Police.state = newState
	Police.timer = 0
	if newState == "tracing" then
		Sounds.startScan1()
	else
		Sounds.stopScan1()
	end

	if newState == "detect" then
		Sounds.startScan2()
	else
		Sounds.stopScan2()
	end

	if newState == "tracing" or newState == "detect" or newState=="found" or newState == "gone" then
		Sounds.playPolice()
	end

	if Police.mode == "request" then
		if newState == "alert" or newState == "aborting" then
			Sounds.playPolice()
		end
	end

	if newState == "found" then
		Sounds.playCaught()
	end
end

function Police.draw()
	if not Police.active then
		return
	end

	
--	if Police.state == "safe" then
--		love.graphics.print("IDLE", Police.x, Police.y)
--	elseif Police.state == "alert"  then
--		love.graphics.print("ALERT", Police.x, Police.y)
--	elseif Police.state == "tracing"  then
--		love.graphics.print("TRACING", Police.x, Police.y)
--	elseif Police.state == "detect"  then
--		love.graphics.print("DETECT", Police.x, Police.y)
--	elseif Police.state == "found" then
--		love.graphics.print("FOUND", Police.x, Police.y)
--	end
	
	if Police.state == "tracing" or Police.state == "detect"  then
		love.graphics.setColor(0,192,255)
		love.graphics.print("CYBERPOLICE COMMUNICATION:\nSuspicious activity detected in sector #"..Sector.name, Police.x, Police.y)
		if Police.blink then
			love.graphics.setColor(255,128,0)
			love.graphics.print("SCANNING SECTOR", Police.x, Police.y + 17*2)
		end

		-- scan bar
--		if not Cracking.aboutToBeDone then
			local barWidth = 100
			local barX = love.graphics.getWidth() - barWidth
			if Police.state == "tracing" then
				love.graphics.setColor(0,192,160,64)
				barX = barX * (1 - Police.timer / Police.traceTime)
			elseif Police.state == "detect" then
				love.graphics.setColor(192,160,0,64)
				barX = barX * Police.timer / Police.detectTime
			end
			love.graphics.rectangle("fill", barX, 0, barWidth, love.graphics.getHeight())
--		end

	elseif Police.state == "alert" then
		if Police.mode == "request" then
			love.graphics.setColor(0,192,255)
			love.graphics.print("CYBERPOLICE COMMUNICATION:\nRoutine scan protocol\nRequesting access to sector #"..Sector.name, Police.x, Police.y)
			if Police.blink then
				love.graphics.setColor(255,128,0)
				love.graphics.print("PRESS ANY KEY TO GRANT ACCESS", Police.x, Police.y + 17*3)
			end
		end
	elseif Police.state == "aborting" then
		if Police.mode == "request" then
			love.graphics.setColor(0,192,255)
			love.graphics.print("CYBERPOLICE COMMUNICATION:\nAccess to sector #"..Sector.name.." DENIED", Police.x, Police.y)
			if Police.blink then
				love.graphics.setColor(255,128,0)
				love.graphics.print("RETURNING TO HEADQUARTERS", Police.x, Police.y + 17*2)
			end
		end
	elseif Police.state == "found" or Police.state == "reboot" then
		if Police.mode == "warning" then
			local blackOverlayOpacity = 1
			if Police.state == "found" then
				 blackOverlayOpacity = (Police.timer / Police.foundTime)
			end
			love.graphics.setColor(0,0,0, 255 * blackOverlayOpacity)
			love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())
		end
		love.graphics.setColor(0,192,255)
		love.graphics.print("CYBERPOLICE COMMUNICATION:", Police.x, Police.y)
		love.graphics.setColor(255,0,0)
		love.graphics.print("INTRUSION DETECTED IN SECTOR #"..Sector.name, Police.x, Police.y + 17)
		if Police.mode == "warning" then		
			if Police.blink then
				love.graphics.setColor(255,128,0)
				love.graphics.print("THIS IS A WARNING", Police.x, Police.y+17*2)
			end
				love.graphics.setColor(255,128,0)
				love.graphics.print("RETURN TO PUBLIC AREA OF THE SERVER.\nNEXT TIME YOU WILL BE DISCONNECTED",Police.x, Police.y+17*3)		

		else
			if Police.blink then
				love.graphics.print("SENDING PATROL", Police.x, Police.y + 17*2)
			end
			-- red overlay
			if Police.state == "found" then
				local redOverlayOpacity = 1 - (Police.timer / Police.foundTime)
				love.graphics.setColor(255,0,0, 64 * redOverlayOpacity)
				love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())
			end
		end

	elseif Police.state == "gone" then
		love.graphics.setColor(0,192,255)
		love.graphics.print("CYBERPOLICE COMMUNICATION:\nEverything in order.  Proceed", Police.x, Police.y)
	end
end

