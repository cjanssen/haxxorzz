Sector = {}

function Sector.init()
	Sector.number = 1
	Sector.reload()
	Sector.firsttime = true
end

function Sector.reload()
	Sector.name = randomWord(3)
	Cracking.init()
	Police.init()
	Endgame.deactivate()
--	Endgame.activate()
	Sector.hackbarVisible = true
	if Sector.number == 1 then
		Strings.setup({
		"","","",
		"          haXXorZZ",
		"          by Christiaan Janssen",
		"          Berlin Mini Game Jam June 2013",
		"",
		"",
		"",
		"",
		"",
		"",
		"                                 __Start"
		})
		Police.active = false
	elseif Sector.number == 2 then
		Strings.setup({
		"CYBERSHOGUN SYSTEMS LTD.",
		"------------------------",
		"Welcome to the Cybershogun Systems public server!",
		"Current date is Monday, #ERROR-53FA3: DIVISION BY ZERO,",
		"the weather forecast for today is SUNNY -144 degrees Celsius",
		"",
		"Please, enter an option to proceed:",
		"[1] Display available services",
		"[2] Request a ticket",
		"[3] Browse catalog",
		"[4] Purchase item from catalog",
		"[5] Contact customer service",
		"[6] Admin login",
		""
		})
		if Sector.firsttime then
			Sector.firsttime = false
		else
			Strings.skipAll()
		end
--	Strings.skipAll()
	elseif Sector.number == 3 then
		Strings.setup({
		"CYBERSHOGUN INTRANET",
		"",
		"Hello employee #"..randomWord(3+math.random(4)),
		"",
		"You have (4) new mails",
		"",
		"Mail #1",
		"From:    Mom (lucilleXXX@hotmeal.com)",
		"To:      me",
		"Subject: Weekend trip",
		"Content: Hello darling! How are you?",
		"         Dad and I are preparing our trip to Venice.  We're so excited!",
		"         But daddy's camera is broken.  Could you come by tonight and have a look? I'm sure that",
		"         our smart son will figure out how to repair it.  The photos of his retirement party are",
		"         still in it, but we don't want to bring it to the shop, you know why.",
		"         Kisses, mum.",
		"",
		"Mail #2",
		"From:    Superboss (bill.jobs@cybershogun.com)",
		"To:      Staff (staff@cybershogun.com)",
		"Subject: Strategy newsletter February",
		"Content: Dear employees,",
		"         Congratulations! This year we've increased sales by 12%. Despite the compe"..randomWord(10),
		randomWord(93),
		randomWord(50).."...........................................",
		"........#ERROR-930CC: DATA CORRUPTED.  PLEASE REBOOT SERVER"
		})
		Cracking.generatePassword("cauliflower", 15, 470, 11)
		Police.active = true
		Police.traceTime = 2.5
		Police.foundTime = 8
		Police.mode = "warning"
	elseif Sector.number == 4 then
		Strings.setup({
			"CYBERSHOGUN INTRANET",
			"",
			"Welcome Admin!",
			"",
			"If you are not an administrator of the system log off immediately.",
			"Trespassing virtual private property is a federal offense",
			"under law #"..randomWord(8).." section "..randomWord(3)..".",
			"Violators will be prosecuted to the full extent of the law.",
			"",
			"Please enter (Y) if you possess legal access rights to this section of the intranet",
			"Please enter (N) if you are violating the law.  In that case please download confession form #"..randomWord(15),
			"from the documents section of the intranet and send a filled copy to our legal department.",
			"                                    ",
			"                                    ",
			"ILLEGAL INTRUSION DETECTED.  CONTACTING CYBERPOLICE HOTLINE.",
			"Please remain in your seat while a detention squad is located.",
			"Have a nice day."
		})
		Cracking.generatePassword("tobecontinu3333", 15, 400, 8)
		Cracking.generatePassword("PurpleBunniesInMyMind", 15, 470, 5)
		Police.active = true
		Police.safeTime = 5
		Strings.delay = 1.3
		Strings.callback = function() Police.state = "found" end
	elseif Sector.number == 5 then
		Strings.setup({
			"SECRET FILE REPOSITORY",
			"",
			"Please select an option",
			"[1] Open secret file",
			"[2] Delete secret file",
			"[3] Download secret file",
			"[4] Print test sheet",
			"[5] Help",
			"[6] None of the other options",
			""
		})
		Strings.delay = 18
		Strings.callback = function()
			Strings.currentString = string.sub(Strings.currentString,1,string.len(Strings.currentString)-1)..
			"\nUNAUTORIZED USE. CONTACTING CYBERPOLICE\n"..Strings.cursorChar
			Police.state = "found"
		end
		Police.active = true
		Police.safeTime = 6
		Cracking.generatePassword("password123", 300, 400, 3)
		Cracking.generatePassword("clubmate", 15, 470, 5)
		Cracking.generatePassword("SmokingHot69", 750, 150, 14)
		Cracking.generatePassword("1337H4X0R", 655, 540, 5)
	elseif Sector.number == 6 then
		Strings.setup({
			"",
			"DOWNLOADING SECRET FILE",
			"",
			"",
			"",
			"[=======================================================]",
			"                                  100% DOWNLOAD COMPLETE",
			"",
			"",
			"Log out? (Y/N)"
			})
		Strings.letterDelay = 0.9
		Strings.jumpToLine(6,1)
		Strings.putCallbackAtLine(7, function() Strings.letterDelay = 0.04 end)
		Police.active = true
		Police.mode = "request"
		Police.safeTime = 8
		Police.alertTime = 6
		Police.abortTime = 4
		Strings.callback = function() Police.lastcall = true end
	elseif Sector.number == 7 then
		Strings.setup({
			"",
			"",
			"       to be continued...",
		})
		Strings.letterDelay = 0.25
	end
end

function Sector.update( dt )
	if Endgame.state == "finished" then
		restartGame()
		return
	end
	if Police.state == "found" or Police.state == "reboot" then
		Strings.block()
		if Police.state == "reboot" then
			if Police.mode == "warning" then
				Sector.number = 2
				Sector.reload()
			else
				Endgame.activate()
			end
		end
	end
	if Sector.number == 4 and Cracking.crackedCount==2 then
		Strings.block()
	end
end

function Sector.draw()
end

function Sector.advance()
	Sector.number = Sector.number + 1
	Sector.reload()
end

function Sector.keyPressed()
	-- todo: for the web version mouseinput will be necessary
	if Police.state ~= "safe" then
--		Sector.number = 1
--		Sector.reload()
		return false
	end

	if Sector.number == 1 then
		Sector.advance()
		return true
	elseif Sector.number == 2 then
		if Strings.done and table.getn(Cracking.list) == 0 then
			Cracking.generatePassword("CYBERSHOGUN1234", 15, 300, 5)
			return true
		elseif Cracking.done then
			Sector.advance()
			return true
		end
	elseif Sector.number == 3 then
		if Cracking.done then
			if string.len(Strings.sourceString) > 0 then
				Strings.skipAll()
			else
				Sector.advance()
			end
			return true
		end
	elseif Sector.number == 4 then
		if Cracking.done then
			Sector.advance()
			return true
		end
	elseif Sector.number == 5 then
		if Cracking.done then
			Sector.advance()
			return true
		end
	elseif Sector.number == 6 then
		if Strings.done then
			Sector.advance()
			return true
		end
	elseif Sector.number == 7 then
		if Strings.done then
			restartGame()
			return true
		end
	end
	return false
end

