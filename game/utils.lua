function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end

function randomChar()
	-- letters and numbers
	local n = math.random(24+24+10)
	if n<11 then
		return string.char(string.byte("0") + n - 1)
	end
		
	n = n - 10
	if n < 25 then
		return string.char(string.byte("A") + n - 1)
	end

	n = n - 24
	return string.char(string.byte("a") + n - 1)
end

function randomWord(wordlength)
	local word = ""
	for i=1,wordlength do
		word = word..randomChar()
	end
	return word
end

