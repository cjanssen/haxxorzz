Strings = {}

function Strings.init()
end

function Strings.setup(initString)
	-- basic settings
	Strings.color = {0,255,0}
	Strings.x = 15
	Strings.y = 15
	Strings.letterDelay = 0.04
	Strings.timer = 0
	Strings.cursorChar = string.char(19)

	-- internal strings
	Strings.refStrings = initString
	Strings.sourceString = table.concat(Strings.refStrings,"\n")
	Strings.currentString = Strings.cursorChar

	-- state
	Strings.done = false
	Strings.active = true

	-- special advance
	Strings.static = false
	Strings.typeAdvance = false
	Strings.specialAdvance = false
	Strings.advanceSpeed = 5
	Strings.keyWasPressed = false

	-- callbacks
	Strings.delay = 0
	Strings.callback = nil
	Strings.insideCallbacks = {}
end

function Strings.jumpToLine(l, chars)
	local cc = chars or 0
	Strings.sourceString = ""
	Strings.currentString = ""
	for i=1,table.getn(Strings.refStrings) do
		if i<l then
			Strings.currentString = Strings.currentString..Strings.refStrings[i].."\n"
		else
			if i==l and cc>0 then
				Strings.currentString = Strings.currentString..string.sub(Strings.refStrings[i],1,cc)
				Strings.sourceString = string.sub(Strings.refStrings[i],cc+1,string.len(Strings.refStrings[i])).."\n"
			else
				Strings.sourceString = Strings.sourceString..Strings.refStrings[i].."\n"
			end
		end
	end
	Strings.currentString = Strings.currentString..Strings.cursorChar
end

function Strings.putCallbackAtLine(l, callback)
	local charcount = 0
	for i=1,l-1 do
		charcount = charcount + string.len(Strings.refStrings[i]) + 1
	end
	if charcount == 0 then charcount = 1 end
	table.insert(Strings.insideCallbacks, {charcount, callback})
end

function Strings.update( dt )
	if not Strings.active then
		return
	end
	if Police.state == "found" then
		return
	end

	if Strings.specialAdvance and Strings.keyWasPressed then
		Strings.advanceText(Strings.advanceSpeed)
	end

	if not Strings.static then
		Strings.timer = Strings.timer + dt
	end
	while Strings.timer >= Strings.letterDelay and not Strings.done do
		Strings.timer = Strings.timer - Strings.letterDelay
		-- play the sound
		local currentCharCode = string.byte(Strings.sourceString)
		if currentCharCode ~= 32 and currentCharCode ~= 10 then
			Sounds.playChar()
		end

		-- cut the cursor
		Strings.currentString = string.sub(Strings.currentString,1,string.len(Strings.currentString)-1)..
			string.sub(Strings.sourceString,1,1)..Strings.cursorChar
		Strings.sourceString = string.sub(Strings.sourceString,2,string.len(Strings.sourceString))

		if string.len(Strings.sourceString) == 0 then
			if not Strings.done then
				Strings.done = true
				if Strings.callback and Strings.delay <= 0 then
					Strings.callback()
				end
			else
				if Strings.callback and Strings.delay > 0 then
					Strings.delay = Strings.delay - dt
					if Strings.delay <= 0 then
						Strings.callback()
					end
				end
			end			
			return
		end

		for i=1,table.getn(Strings.insideCallbacks) do
			if string.len(Strings.currentString) == Strings.insideCallbacks[i][1] then
				Strings.insideCallbacks[i][2]()
			end
		end
	end	
end

function Strings.draw()
	love.graphics.setColor(Strings.color[1],Strings.color[2],Strings.color[3])
	love.graphics.print(Strings.currentString, Strings.x, Strings.y)
end

function Strings.skipAll()
	Strings.currentString = string.sub(Strings.currentString,1,string.len(Strings.currentString)-1)..
			Strings.sourceString..Strings.cursorChar
	Strings.sourceString = ""
end

function Strings.advanceText(n)
	local num = n or 1
	Strings.timer = Strings.timer + Strings.letterDelay * num
end

function Strings.block()
	Strings.active = false
end

function Strings.keyPressed()
	Strings.keyWasPressed = true
	if Strings.canAdvance and not Strings.done then
		Strings.advanceText()
		return true
	end
	return false
end

function Strings.keyReleased()
	Strings.keyWasPressed = false
end


