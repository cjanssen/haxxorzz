Cracking = {}

function Cracking.init()
	Cracking.list = {}
	Cracking.crackedCount = 0
	Cracking.done = false
	Cracking.aboutToBeDone = false
	Cracking.generalTimer = 0
	Cracking.keylessdelay = 0.4
end

function Cracking.generatePassword(newPassword, _x, _y, difficulty)
	local pwset = {
		refreshRate = 0.05,
		timer = 0,
		keyCount = 0,
		keyStep = difficulty,
		pendingLetters = string.len(newPassword),
		x = _x,
		y = _y,
		falsePassword = "",
		showPassword = "",
		foundLetters = {},
		currentPassword = newPassword,
		cracked = 0,
		successDelay = 0.5,

		-- click feedback bar
		barLen = 0,
		barOpacity = 0,
		charSuccess = 0,
		pwsDelay = 1,
		pwsOpacity = 0
	}

	for i=1,string.len(pwset.currentPassword) do
		pwset.foundLetters[i] = false
	end
--	return pwset
	table.insert(Cracking.list, pwset)
end

function Cracking.keyPressed()
	if table.getn(Cracking.list) == 0 then
		return false
	end
	if Cracking.aboutToBeDone then
		Cracking.generalTimer = 0
	end

	if Police.state == "found" or Police.state == "reboot" or Endgame.active then
		return false
	end
	if Cracking.crackedCount == table.getn(Cracking.list) then
		return false
	end
	item = { cracked = 2 }
	while item.cracked ~= 0 do
		item = Cracking.list[math.random(table.getn(Cracking.list))]
	end

--	item = Cracking.list[math.random(table.getn(Cracking.list))]
	item.keyCount = item.keyCount + 1

	-- feedback bar
	item.barLen = item.keyCount / item.keyStep
	if item.barLen > 1 then item.barLen = 1 end
	item.barOpacity = 1

	if item.keyCount >= item.keyStep and item.pendingLetters > 0 then
		item.keyCount = item.keyCount - item.keyStep
		item.pendingLetters = item.pendingLetters - 1
		local n = math.random(string.len(item.currentPassword))
		local i = 0
		while n > 0 do
			i = (i % string.len(item.currentPassword)) + 1
			if not item.foundLetters[i] then
				n = n - 1
			end
		end
		item.foundLetters[i] = true
		item.charSuccess = 1

		if item.pendingLetters == 0 then
			Sounds.playCracked()
			item.cracked = 1
			item.timer = 0
			Cracking.crackedCount = Cracking.crackedCount + 1
			item.pwsOpacity = 1
		else
			Sounds.playCharFound()
		end
	end

	return true
end

function Cracking.update( dt )
	-- animate
	for i,item in ipairs(Cracking.list) do
		-- these animations should proceed no matter what
		item.barOpacity = decreaseExponential(dt, item.barOpacity, 0.85)	
		item.charSuccess = decreaseExponential(dt, item.charSuccess, 0.9)	
		item.pwsOpacity = decreaseExponential(dt, item.pwsOpacity, 0.9)
	end

	if Police.state == "found" or Police.state == "reboot" or Endgame.active then
		return
	end

	local cypherPlayed = false
	for i,item in ipairs(Cracking.list) do
		item.timer = item.timer + dt
		if item.cracked == 0 then			
			if item.timer >= item.refreshRate then
				if not cypherPlayed then
					Sounds.playCypher()
					cypherPlayed = true
				end
				item.timer = item.timer - item.refreshRate
				item.falsePassword = ""
				for i=1,string.len(item.currentPassword) do
					item.falsePassword = item.falsePassword .. randomChar()
				end
			end
		elseif item.cracked == 1 then
			if item.timer >= item.successDelay then
				item.cracked = 2
				if Cracking.crackedCount == table.getn(Cracking.list) then
					Cracking.aboutToBeDone = true
					Cracking.generalTimer = 0
					Police.acceptsKeys = false
				end
				if item.callback then
					item.callback()
				end
			end
		end
	end

	if Cracking.aboutToBeDone then
		Cracking.generalTimer = Cracking.generalTimer + dt
		if Cracking.generalTimer >= Cracking.keylessdelay then
			Cracking.done = true
		end
	end

	
end

function Cracking.draw()
	local enterMSG = "ENTER PASSWORD:"
	local msgW = font:getWidth(enterMSG)

	for i,item in ipairs(Cracking.list) do
		if not Sector.hackbarVisible and item.charSuccess > 0.001 then
			-- single char cracked
			love.graphics.setColor(255,192,0,255*item.charSuccess)
			love.graphics.rectangle("fill",item.x,item.y+34, font:getWidth(item.currentPassword), 17)
		end

		if item.pwsOpacity > 0.001 then
			-- full word cracked
			local fraction = math.pow(1-item.pwsOpacity,4)
			love.graphics.setColor(255,192,0,255*item.pwsOpacity)
			local rw = 80 + fraction*320
			local rh = 16 + fraction*160
			love.graphics.rectangle("fill", item.x + msgW*0.5 - rw*0.5, item.y + 43 - rh*0.5, rw, rh)
		end
		

		love.graphics.setColor(0, 192, 255)
		love.graphics.print(enterMSG, item.x, item.y + 17)
		if item.pendingLetters > 0 then
			for i=1,string.len(item.currentPassword) do
				if item.foundLetters[i] then
					love.graphics.setColor(0, 255, 0)
					love.graphics.print(string.sub(item.currentPassword,i,i), item.x + (i-1)*10, item.y + 34)
				else
					love.graphics.setColor(255, 0, 0)
					love.graphics.print(string.sub(item.falsePassword,i,i), item.x + (i-1)*10, item.y + 34)
				end
			end
		else
			love.graphics.setColor(0, 255, 0)
			love.graphics.print(item.currentPassword, item.x, item.y + 34)
			love.graphics.setColor(255, 192, 0)
			love.graphics.print("PASSWORD CRACKED", item.x, item.y + 51)
		end

		-- feedback bar disabled (it was too much)
		if Sector.hackbarVisible then
			if item.barOpacity > 0.001 then
				love.graphics.setColor(96 * (1-item.barLen), 96 + (72-96)*item.barLen, 0, 255*item.barOpacity)
				love.graphics.rectangle("fill", item.x, item.y+2,  msgW, 12)
				love.graphics.setColor(255*(1-item.barLen), 255 + (192-255)*item.barLen, 0, 255*item.barOpacity)
				love.graphics.rectangle("fill", item.x, item.y+2, msgW * item.barLen, 12)
			end
		end
	end
end
