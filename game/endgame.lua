Endgame = {}

function Endgame.init()
	-- generate white noise (3 screens)
	Endgame.noise = {}
	Endgame.noiseIndex = 1
	Endgame.frameTime = 0
	Endgame.frameDelay = 1/60

	Endgame.active = false
	Endgame.generator = coroutine.create(Endgame.generateAllNoiseScreens)
	Endgame.state = "start"

	Endgame.generalTime = 0

	Endgame.fadeinDelay = 5
	Endgame.fadeinPause = 1.5
	Endgame.scaleVertDelay = 0.13
	Endgame.scaleHorzDelay = 0.3
	Endgame.scalePause = 0.2
	Endgame.fadeoutDelay = 3
	Endgame.fadeoutPause = 3

	Endgame.minVertScale = 2/love.graphics.getHeight()
	Endgame.minHorzScale = 2/love.graphics.getWidth()
end

function Endgame.generateAllNoiseScreens()
	local slottedTime = 1/30
	local startTime = love.timer.getTime()
	for i=1,10 do
		local canvas = love.graphics.newCanvas()
	
		for x=0,love.graphics.getWidth() do
			love.graphics.setCanvas(canvas)
			for y=0,love.graphics.getHeight() do
				local c = (math.random(17)-1)*16
				love.graphics.setColor(c,c,c,255)
				love.graphics.rectangle("fill",x,y,1,1)
			end
			Endgame.currentY = x
			love.graphics.setCanvas()
			if love.timer.getTime() - startTime >= slottedTime then
				coroutine.yield()
				startTime = love.timer.getTime()
			end
		end
		table.insert(Endgame.noise, canvas)
	end	
end

function Endgame.generateNoiseScreen()
	local canvas = love.graphics.newCanvas()
	love.graphics.setCanvas(canvas)
	
	for x=0,love.graphics.getWidth() do
		for y=0,love.graphics.getHeight() do
			local c = (math.random(17)-1)*16
			love.graphics.setColor(c,c,c,255)
			love.graphics.rectangle("fill",x,y,1,1)
		end
	end

	love.graphics.setCanvas()
	return canvas
end

function Endgame.activate()
	if not Endgame.active then
		Endgame.active = true
		Endgame.generalTime = 0
		Endgame.state = "start"
	end
end

function Endgame.deactivate()
	Endgame.active = false
	Endgame.generalTime = 0
	Endgame.state = "start"
end

function Endgame.isComputing()
	return coroutine.status(Endgame.generator) ~= 'dead'
end

function Endgame.update(dt)
	if Endgame.isComputing() then
		coroutine.resume(Endgame.generator)
		return
	end

	if Endgame.active then
		Endgame.frameTime = Endgame.frameTime + dt
		if Endgame.frameTime >= Endgame.frameDelay then
			Endgame.frameTime = Endgame.frameTime - Endgame.frameDelay
			Endgame.noiseIndex = (Endgame.noiseIndex % table.getn(Endgame.noise)) + 1
		end

		Endgame.generalTime = Endgame.generalTime + dt
		if Endgame.state == "start" then
			Endgame.state = "fadein"
			Sounds.setSnowVolume(0)
			Sounds.playSnow()
		elseif Endgame.state == "fadein" then
			local volume = Endgame.generalTime / Endgame.fadeinDelay
			if volume > 1 then volume = 1 end
			Sounds.setSnowVolume(volume)
			if Endgame.generalTime >= Endgame.fadeinDelay + Endgame.fadeinPause then
				Endgame.generalTime = 0
				Endgame.state = "verticalScale"
				Sounds.stopSnow()
				Sounds.playPowdown()
			end
		elseif Endgame.state == "verticalScale" then
			if Endgame.generalTime >= Endgame.scaleVertDelay then
				Endgame.generalTime = 0
				Endgame.state = "horizontalScale"
			end
		elseif Endgame.state == "horizontalScale" then
			if Endgame.generalTime >= Endgame.scaleHorzDelay + Endgame.scalePause then
				Endgame.generalTime = 0
				Endgame.state = "fadeout"
			end
		elseif Endgame.state == "fadeout" then
			if Endgame.generalTime >= Endgame.fadeoutDelay + Endgame.fadeoutPause then
				Endgame.state = "finished"
			end
		end
	end
end

function Endgame.predraw()
--	love.graphics.push()
--	if Endgame.active then

--		if Endgame.state == "verticalScale" then
--			local fraction = 1 - Endgame.generalTime / Endgame.scaleVertDelay
--			if fraction < Endgame.minVertScale then
--				fraction = Endgame.minVertScale
--			end

--			love.graphics.translate(0,love.graphics.getHeight()* 0.5 * (1 - fraction))
--			love.graphics.scale(1,fraction)
--		elseif Endgame.state == "horizontalScale" then
--			local fraction = 1 - Endgame.generalTime / Endgame.scaleHorzDelay
--			if fraction < Endgame.minHorzScale then
--				fraction = Endgame.minHorzScale
--			end

--			love.graphics.translate(love.graphics.getWidth()* 0.5 * (1 - fraction), love.graphics.getHeight()* 0.5 * (1 - Endgame.minVertScale))
--			love.graphics.scale(fraction, Endgame.minVertScale)
--		elseif Endgame.state == "finished" then
--			love.graphics.translate(love.graphics.getWidth()* 0.5 * (1 - minHorzScale), 
--									love.graphics.getHeight()* 0.5 * (1 - Endgame.minVertScale))
--			love.graphics.scale(Endgame.minHorzScale, Endgame.minVertScale)
--		end
--	end
end

function Endgame.draw()
	if Endgame.active then
		if Endgame.state == "start" then
			return
		end

		-- draw noise
		local noiseFraction = 1
		if Endgame.state == "fadein" then
			noiseFraction = Endgame.generalTime / Endgame.fadeinDelay
			if noiseFraction > 1 then noiseFraction = 1 end
		end
		love.graphics.setColor(255,255,255,255 * noiseFraction)
		if not Endgame.isComputing() then
			love.graphics.draw(Endgame.noise[Endgame.noiseIndex],0,0)
		end

		-- horizontal bars
		local hbheight = 0.5 * love.graphics.getHeight() - 1
		if Endgame.state == "fadein" then
			hbheight = 0		
		elseif Endgame.state == "verticalScale" then
			hbheight = (Endgame.generalTime / Endgame.scaleVertDelay) * 0.5 * love.graphics.getHeight() - 1
			if Endgame.generalTime > Endgame.scaleVertDelay then
				hbheight = 0.5 * love.graphics.getHeight() - 1
			end
		else
			love.graphics.setColor(255,255,255)
			love.graphics.rectangle("fill",0,hbheight,love.graphics.getWidth(),2)
		end
		love.graphics.setColor(0,0,0,255)
		love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), hbheight)
		love.graphics.rectangle("fill", 0, love.graphics.getHeight() - hbheight, love.graphics.getWidth(), hbheight)

		-- vertical bars
		local hbwidth = 0.5 * love.graphics.getWidth() - 1
		if Endgame.state == "fadein" or Endgame.state == "verticalScale" then
			hbwidth = 0		
		elseif Endgame.state == "horizontalScale" then
			hbwidth = (Endgame.generalTime / Endgame.scaleHorzDelay) * 0.5 * love.graphics.getWidth() - 1
			if Endgame.generalTime > Endgame.scaleHorzDelay then
				hbwidth = 0.5 * love.graphics.getWidth() - 1
			end
		end
		love.graphics.setColor(0,0,0,255)
		love.graphics.rectangle("fill", 0, 0, hbwidth, love.graphics.getHeight())
		love.graphics.rectangle("fill", love.graphics.getWidth() - hbwidth, 0, hbwidth, love.graphics.getHeight())


		-- dark overlay
		local overlayFraction = 0
		if Endgame.state == "fadeout" then
			overlayFraction = Endgame.generalTime / Endgame.fadeoutDelay
			if overlayFraction > 1 then overlayFraction = 1 end
		end
		if Endgame.state == "finished" then
			overlayFraction = 1
		end
		love.graphics.setColor(0,0,0,overlayFraction * 255)
		love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())
	end
--	love.graphics.pop()
end

