function include( filename )
	love.filesystem.load( filename )()
end


function loadDependencies()
	include("utils.lua")
	include("sector.lua")
	include("strings.lua")
	include("cracking.lua")
	include("police.lua")
	include("endgame.lua")
	include("sounds.lua")
end

function love.load()
	loadDependencies()
	love.mouse.setVisible(false)
	love.setFont()
	Sector.init()
	Endgame.init()
	Sounds.init()

--	Strings.init()
--	Cracking.init()
--	Police.init()
end

function restartGame()
	Endgame.deactivate()
	Sector.number = 1
	Sector.firsttime = true
	Sector.reload()
end

function love.draw()
	Endgame.predraw()
	Strings.draw()
	Cracking.draw()
	Sector.draw()
	Police.draw()	
	Endgame.draw()
end

function love.update(dt)
	if dt > 0.2 then
		-- skip long jumps
		return
	end
	Sector.update(dt)
	Strings.update(dt)
	Cracking.update(dt)
	Police.update(dt)
	Endgame.update(dt)
end

function love.setFont()
	font = love.graphics.newFont("fnt/ocraextended.ttf", 16)
	love.graphics.setFont(font)
end

function love.keypressed( key )
	if key == "escape" then
		love.event.push("quit")
		return
	end
	if key == "f5" then
		Sector.reload()
		return
	end

	-- if key == "return" then
	-- 	if love.keyboard.isDown("lalt","ralt") then
	-- 		love.graphics.toggleFullscreen()
	-- 	end
	-- end

	local keyhandlers = { Strings, Sector, Police, Cracking }
	for i,v in ipairs(keyhandlers) do
		if (v.keyPressed()) then
			return
		end
	end
end

function love.keyreleased(key)
	Strings.keyReleased()
end

