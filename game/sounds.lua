Sounds = {}

function Sounds.init()
	Sounds.charSounds = Sounds.initSet("snd/charsound.ogg",6)
	Sounds.cypherSounds = Sounds.initSet("snd/cypher.ogg",6)
	Sounds.correctCharSounds = Sounds.initSet("snd/letterright.ogg",3)	
	Sounds.hackedWord = Sounds.initSet("snd/passhacked.ogg",3)
	Sounds.snow = love.audio.newSource("snd/pinknoise.ogg")
	Sounds.powdown = love.audio.newSource("snd/powdown.ogg")
	Sounds.scan1 = love.audio.newSource("snd/scan1.ogg")
	Sounds.scan1:setLooping(true)
	Sounds.scan1:setVolume(0.5)
	Sounds.scan2 = love.audio.newSource("snd/scan2.ogg")
	Sounds.scan2:setLooping(true)
	Sounds.scan2:setVolume(0.5)
	Sounds.showPolice = love.audio.newSource("snd/computa.ogg")
	Sounds.caught = love.audio.newSource("snd/caught.ogg")
end

function Sounds.initSet(fname, cnt)
	local ret = { index = 1 }
	for i=1,cnt do
		table.insert(ret, love.audio.newSource(fname))
	end
	return ret
end

function Sounds.playFromSet(set)
	local n = table.getn(set)
	local maxtries = n
	while not set[set.index]:isStopped() and maxtries > 0 do
		set.index = (set.index % n) + 1
		maxtries = maxtries - 1
	end
	if maxtries > 0 then
		set[set.index]:setVolume(1-Sounds.getSnowVolume())
		set[set.index]:play()
		set.index = (set.index % n) + 1
	end
end

function Sounds.playChar()
	Sounds.playFromSet(Sounds.charSounds)
end

function Sounds.playCypher()
	Sounds.playFromSet(Sounds.cypherSounds)
end

function Sounds.playCharFound()
	Sounds.playFromSet(Sounds.correctCharSounds)
end

function Sounds.playCracked()
	Sounds.playFromSet(Sounds.hackedWord)
end

function Sounds.playSnow()
	Sounds.snow:play()
end

function Sounds.stopSnow()
	Sounds.snow:stop()
end

function Sounds.setSnowVolume(v)
	Sounds.snow:setVolume(v)
end

function Sounds.getSnowVolume(v)
	if not Endgame.active then return 0 end
	if not Sounds.snow:isStopped() then return Sounds.snow:getVolume() end
	return 1
end

function Sounds.playPowdown()
	Sounds.powdown:play()
end

function Sounds.startScan1()
	Sounds.scan1:play()
end

function Sounds.stopScan1()
	Sounds.scan1:stop()
end

function Sounds.startScan2()
	Sounds.scan2:play()
end

function Sounds.stopScan2()
	Sounds.scan2:stop()
end

function Sounds.playPolice()
	Sounds.showPolice:play()
end

function Sounds.playCaught()
	Sounds.caught:play()
end
